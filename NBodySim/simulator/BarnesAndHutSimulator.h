//
// Created by alex on 04.08.2019.
//

#ifndef NBODYSIM_BARNESANDHUTSIMULATOR_H
#define NBODYSIM_BARNESANDHUTSIMULATOR_H

#include <vector>
#include <memory>
#include "../body/Body.h"
#include "../Node.h"

class BarnesAndHutSimulator/* : public SimpleSimulator*/ {
private:
    const double TETA = 0.1;
    std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies; // Общий список тел
    std::shared_ptr<Node> pNode; // Корневой узел

    void recreateNode();

    void calcEdge(std::shared_ptr<std::vector<std::shared_ptr<Body>>> pConstBodies, double &x, double &y, double &z, double &d);
public:
    void update(double dt);
    void calcForce(const std::shared_ptr<Node>& pNode, const std::shared_ptr<Body>& pBody);

    explicit BarnesAndHutSimulator(const std::shared_ptr<std::vector<std::shared_ptr<Body>>>& pBodies);
    ~BarnesAndHutSimulator();
};

#endif //NBODYSIM_BARNESANDHUTSIMULATOR_H
