//
// Created by alex on 04.08.2019.
//

#ifndef NBODYSIM_SECONDLAWSIMULATOR_H
#define NBODYSIM_SECONDLAWSIMULATOR_H


#include <vector>
#include <memory>
#include "../body/Body.h"

class SecondLawSimulator/* : public SimpleSimulator*/ {
private:
    std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies;

public:
    void update(double dt = 1E+11);
    explicit SecondLawSimulator(std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies);
    ~SecondLawSimulator();
};


#endif //NBODYSIM_SECONDLAWSIMULATOR_H
