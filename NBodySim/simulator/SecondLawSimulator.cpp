//
// Created by alex on 04.08.2019.
//

#include "SecondLawSimulator.h"

#include <utility>

void SecondLawSimulator::update(double dt) {
    for (auto i = 0; i < pBodies->size(); i++) {
        (*pBodies)[i]->resetForce();
        for (auto j = 0; j < pBodies->size(); j++) {
            if (i != j) (*pBodies)[i]->addForce((*pBodies)[j]);
        }
    }
    //Then, loop again and update the bodies using timestep dt
    for (auto pBody : *pBodies) {
        pBody->update(dt);
        std::wcout << pBody->toString() << std::endl;
    }
}

SecondLawSimulator::SecondLawSimulator(std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies) : pBodies(std::move(pBodies)) {}

SecondLawSimulator::~SecondLawSimulator() {
    for (auto pBody : *pBodies) {
        pBody.reset();
    }
    pBodies->clear();
    pBodies.reset();
}