//
// Created by alex on 04.08.2019.
//

#include "BarnesAndHutSimulator.h"

/**
 * Перерасчитывает местоположения математических точек через dt сек. времени
 * @param dt sec
 */
void BarnesAndHutSimulator::update(double dt) {
    for (auto & pBody : *pBodies) {
        pBody->resetForce();

        calcForce(pNode, pBody);
    }
    //Then, loop again and update the bodies using timestep dt
    for (auto & pBody : *pBodies) {
        pBody->update(dt);
        std::wcout << pBody->toString() << std::endl;
    }

    recreateNode();
}

/*
 * Пересоздание дерева после каждой итерации
 */
void BarnesAndHutSimulator::recreateNode() {
    // TODO NPE when pBodies clear and delete pointer
    pNode.reset();

    double x, y, z, d;
    calcEdge(pBodies, x, y, z, d);
    pNode = std::make_shared<Node>(x, y, z, d, pBodies);
}

/**
 * Расчитывает силы действующие на тело pBody со стороны pTempNode
 * @param pTempNode
 * @param pBody
 */
void BarnesAndHutSimulator::calcForce(const std::shared_ptr<Node>& pTempNode, const std::shared_ptr<Body>& pBody) {
    pTempNode->calcYourself();

    // TODO Объединить два условия
    if (pTempNode->pBodies->size() == 1) {
        std::shared_ptr<Body> pTempBody = pTempNode->pBody;
        //if (pBody != pTempBody) {
            pBody->addForce(pTempBody);
        //}
    } else {
        std::shared_ptr<Body> pTempBody = pTempNode->pBody;
        // TODO может быть деление на 0?
        if (pTempNode->d / pBody->distanceTo(pTempBody) < TETA) {
            pBody->addForce(pTempBody);
        } else {
            /*
            for all child nodes n
            force += n.CalculateForce(particle)
            end for
            */
            pTempNode->calcInnerNode();
            if (pTempNode->pNode000 != nullptr) {
                calcForce(pTempNode->pNode000, pBody);
            }
            if (pTempNode->pNode001 != nullptr) {
                calcForce(pTempNode->pNode001, pBody);
            }
            if (pTempNode->pNode010 != nullptr) {
                calcForce(pTempNode->pNode010, pBody);
            }
            if (pTempNode->pNode011 != nullptr) {
                calcForce(pTempNode->pNode011, pBody);
            }
            if (pTempNode->pNode100 != nullptr) {
                calcForce(pTempNode->pNode100, pBody);
            }
            if (pTempNode->pNode101 != nullptr) {
                calcForce(pTempNode->pNode101, pBody);
            }
            if (pTempNode->pNode110 != nullptr) {
                calcForce(pTempNode->pNode110, pBody);
            }
            if (pTempNode->pNode111 != nullptr) {
                calcForce(pTempNode->pNode111, pBody);
            }
        }
    }
}

/**
 * Вычисляем границы моделируемой системы
 * Создаём корневую ноду
 * @param pBodies указатель на вектор тел
 */
BarnesAndHutSimulator::BarnesAndHutSimulator(const std::shared_ptr<std::vector<std::shared_ptr<Body>>>& pBodies) : pBodies(pBodies) {
    if (!pBodies) {
        throw std::runtime_error("ERROR: Cannot create galaxy");
    }

    if ((*pBodies).empty()) {
        throw std::runtime_error("ERROR: pBodies is empty!");
    }

    double x, y, z, d;
    calcEdge(pBodies, x, y, z, d);

    pNode = std::make_shared<Node>(x, y, z, d, pBodies);

    std::wcout << L"B&H constructor" << std::endl;
    std::wcout << L"x = " << x << std::endl;
    std::wcout << L"y = " << y << std::endl;
    std::wcout << L"z = " << z << std::endl;
    std::wcout << L"d = " << d << std::endl;
}

// TODO Реализовать рекурсивную отчистку памяти
BarnesAndHutSimulator::~BarnesAndHutSimulator() {
    pNode.reset();
    for (auto pBody : *pBodies) {
        pBody.reset();
    }
    pBodies->clear();
    pBodies.reset();
}

/**
 * Пересчитывает границы моделируемого участка
 * @param pConstBodies небесные тела
 * @param x
 * @param y
 * @param z
 * @param d сторона куба
 */
void BarnesAndHutSimulator::calcEdge(const std::shared_ptr<std::vector<std::shared_ptr<Body>>> pConstBodies,
        double &x, double &y, double &z, double &d) {
    double x_max, x_min, y_max, y_min, z_max, z_min, d_x, d_y, d_z;

    x_max = x_min = (*pConstBodies)[0]->getRX();
    y_max = y_min = (*pConstBodies)[0]->getRY();
    z_max = z_min = (*pConstBodies)[0]->getRZ();
    for (auto i = 1; i < pConstBodies->size(); i++) {
        if ((*pConstBodies)[i]->getRX() < x_min) {
            x_min = (*pConstBodies)[i]->getRX();
        }
        if ((*pConstBodies)[i]->getRX() > x_max) {
            x_max = (*pConstBodies)[i]->getRX();
        }
        if ((*pConstBodies)[i]->getRY() < y_min) {
            y_min = (*pConstBodies)[i]->getRY();
        }
        if ((*pConstBodies)[i]->getRY() > y_max) {
            y_max = (*pConstBodies)[i]->getRY();
        }
        if ((*pConstBodies)[i]->getRZ() < z_min) {
            z_min = (*pConstBodies)[i]->getRZ();
        }
        if ((*pConstBodies)[i]->getRZ() > z_max) {
            z_max = (*pConstBodies)[i]->getRZ();
        }
    }

    d_x = fabs(x_max - x_min);
    d_y = fabs(y_max - y_min);
    d_z = fabs(z_max - z_min);

    // Выбираем максимальное расстояние между точками среди всех координат
    d = d_x > d_y ? d_x : d_y;
    d = d > d_z ? d : d_z;

    x = (x_max - x_min) / 2 - d / 2;
    y = (y_max - y_min) / 2 - d / 2;
    z = (z_max - z_min) / 2 - d / 2;
}
