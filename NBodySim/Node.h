//
// Created by alex on 02.08.2019.
//

#ifndef NBODYSIM_NODE_H
#define NBODYSIM_NODE_H

#include "body/Body.h"

#include <vector>
#include <memory>

class Node {
public:
    double d; // Величина сторны квадрата
    double x, y, z; // Минимальные координаты угла куба
    // pNode[X][Y][Z]
    std::shared_ptr<Node> pNode000,
                          pNode001,
                          pNode010,
                          pNode011,
                          pNode100,
                          pNode101,
                          pNode110,
                          pNode111;
    std::shared_ptr<Body> pBody; // Абстрактное тело, представляющее собой совокупность сил, массы и тел внутри принадлежащих этой ноде квадратов
    std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies; // Все тела данного квадрата

    void calcInnerNode(); // Расчитывает внутренние квадраты

    void calcYourself(); // Расчитывает центр масс текущего квадрата

    Node(double x, double y, double z, double d, std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies);
    ~Node();

    std::wstring toString();
};

#endif //NBODYSIM_NODE_H
