### For develop (Ubuntu 20.10)

1. Dowload and instal SDL2

    ```terminal
    sudo apt install libsdl2-dev -y
    ```

2. Dowload and install `SDL_image` library

    ```terminal
    apt-cache search libsdl2-image
    apt-get install libsdl2-image-dev
    ```

3. Install Cmake script for Build

    See [this](https://trenki2.github.io/blog/2017/07/04/using-sdl2-image-with-cmake/) tutorial

### Link

SDL2 tutorial: [1](http://lazyfoo.net/tutorials/SDL/), [2](https://www.amazon.com/SDL-Game-Development-Shaun-Mitchell-ebook/dp/B00DL0CFI6)
