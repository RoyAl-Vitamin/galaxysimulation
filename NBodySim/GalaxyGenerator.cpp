//
// Created by alex on 01.08.2019.
//

#include "GalaxyGenerator.h"

std::shared_ptr<std::vector<std::shared_ptr<Body>>> GalaxyGenerator::galaxyConstructor(GalaxyType gType_) {
    std::shared_ptr<std::vector<std::shared_ptr<Body>>> temp(nullptr);
    switch (gType_) {
        case EARTH_AND_MOON:
        {
            temp = std::make_shared<std::vector<std::shared_ptr<Body>>>(std::initializer_list<std::shared_ptr<Body>> {
                std::make_shared<Body>("assets/earth.png", "earth", L"Земля", 0, 0, 0, 0, 0, 0, 5.97600E+24, 6'371),
                std::make_shared<Body>("assets/moon.png", "moon", L"Луна", 384'399, 0, 0, 0, 1.023, 0, 7.3477E+22, 1'737)
            });
            break;
        }
        case SOLAR_SYSTEM:
        {
            temp = std::make_shared<std::vector<std::shared_ptr<Body>>>(std::initializer_list<std::shared_ptr<Body>> {
                std::make_shared<Body>("assets/sun.png", "sun", L"Солнце", 0, 0, 0, 0, 0, 0, 1989000.0E+24, 696'340),
                std::make_shared<Body>("assets/mercury.png", "mercury", L"Меркурий", 57'910'006, 0, 0, 0, 47.87, 0, 0.32868E+24, 2'439),
                std::make_shared<Body>("assets/venus.png", "venus", L"Венера", 108'199'995, 0, 0, 0, 35.02, 0, 4.81068E+24, 6'051),
                std::make_shared<Body>("assets/earth.png", "earth", L"Земля", 149'599'951, 0, 0, 0, 29.78, 0, 5.97600E+24, 6'371),
                std::make_shared<Body>("assets/mars.png", "mars", L"Марс", 227'939'920, 0, 0, 0, 24.13, 0, 0.63345E+24, 3'389),
                std::make_shared<Body>("assets/ceres.png", "ceres", L"Церера", 413'900'000, 0, 0, 0, 17.88, 0, 0.00094E+24, 473),
                std::make_shared<Body>("assets/jupiter.png", "jupiter", L"Юпитер", 778'330'257, 0, 0, 0, 13.07, 0, 1876.64328E+24, 69'911),
                std::make_shared<Body>("assets/saturn.png", "saturn", L"Сатурн", 1'429'400'028, 0, 0, 0, 9.69, 0, 561.80376E+24, 58'232),
                std::make_shared<Body>("assets/uranus.png", "uranus", L"Уран", 2'870'989'228, 0, 0, 0, 6.81, 0, 86.05440E+24, 25'362),
                std::make_shared<Body>("assets/neptune.png", "neptune", L"Нептун", 4'504'299'579, 0, 0, 0, 5.43, 0, 101.59200E+24, 24'622),
                std::make_shared<Body>("assets/pluto.png", "pluto", L"Плутон", 5'906'376'272, 0, 0, 0, 4.67, 0, 0.01303E+24, 1'188),
                std::make_shared<Body>("assets/haumea.png", "haumea", L"Хаумеа", 7'540'000'000, 0, 0, 0, 4.48, 0, 0.004006E+24, 816),
                std::make_shared<Body>("assets/makemake.png", "makemake", L"Макемаке", 7'900'000'000, 0, 0, 0, 4.41, 0, 0.003E+24, 715),
                std::make_shared<Body>("assets/eris .png", "eris ", L"Эрида", 14'610'000'000, 0, 0, 0, 3.44, 0, 0.0167E+24, 1'163)
            });
            break;
        }
        case OTHER:
            // Генерация галактик
            double r_x_bh = 0, r_y_bh = 0, r_z_bh = 0, mass_bh = 1.9890E+40; // Параметры чёрной дыры
            const int COUNT = 100; // Количество небесных тел
            const double r = 14'610'000'000 * 2; // Радиус генерации
            auto pBlackHole = std::make_shared<Body>("assets/asteroid.png", "asteroid", L"Астероид", r_x_bh, r_y_bh, r_z_bh, 0, 0, 0, mass_bh);
            temp = std::make_shared<std::vector<std::shared_ptr<Body>>>();
            temp->push_back(pBlackHole);
            for (int i = 0; i < COUNT; i++) {
                double r_x, r_y, r_z, mass;
                // Для плоского случая
                mass = rand() * (mass_bh / 2) / RAND_MAX + 1E+20;

                r_x = rand() * (r - r_x_bh) / RAND_MAX + r_x_bh;
                r_y = rand() * (r - r_y_bh) / RAND_MAX + r_y_bh;
                r_z = 0; // TODO

                auto pBody = std::make_shared<Body>("assets/asteroid.png", "asteroid", L"Planet_" + std::to_wstring(i), r_x, r_y, r_z, 0, 0, 0, mass);
                pBody->addForce(pBlackHole);

                double a_x = pBody->getFX() / pBody->getMass(), a_y = pBody->getFY() / pBody->getMass(), a_z = pBody->getFZ() / pBody->getMass();
                pBody->setVX(-a_y); // -+
                pBody->setVY(+a_x); // +-
                pBody->setVZ(0); // TODO

                temp->push_back(pBody);
            }
            break;
    }
    return temp;
}
