//
// Created by alex on 28.07.2019.
//

#include "Body.h"

constexpr double getEps() {
    return 3E-4;
}

constexpr double getSquareEps() {
    return getEps() * getEps();
}

double Body::distanceTo(const std::shared_ptr<Body> pBody) {
    double dx = this->r_x - pBody->r_x;
    double dy = this->r_y - pBody->r_y;
    double dz = this->r_z - pBody->r_z;
    return sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
}

void Body::draw() {}
void Body::clean() {}

void Body::update() {
    Body::update(0);
}

void Body::update(double dt) {
    this->v_x += dt * this->f_x / this->mass;
    this->v_y += dt * this->f_y / this->mass;
    this->v_z += dt * this->f_z / this->mass;

    this->r_x += dt * this->v_x;
    this->r_y += dt * this->v_y;
    this->r_z += dt * this->v_z;

    currentFrame = int(((SDL_GetTicks() / 100) % 6));
}

void Body::resetForce() {
    this->f_x = 0.0;
    this->f_y = 0.0;
    this->f_z = 0.0;
}

void Body::addForce(const std::shared_ptr<Body> pBody) {
    double dx = pBody->r_x - this->r_x;
    double dy = pBody->r_y - this->r_y;
    double dz = pBody->r_z - this->r_z;
    double dist = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
    double F = (G * this->mass * pBody->mass) / (pow(dist, 2) + getSquareEps());
    if (dist < 1E+2) { return; } // Убираем взаимодействие при слишком близком контакте < 100km
    this->f_x += F * dx / dist;
    this->f_y += F * dy / dist;
    this->f_z += F * dz / dist;
}

Body::Body(std::string path, std::string textureId, std::wstring name, 
        double r_x, double r_y, double r_z, double v_x, double v_y, double v_z, 
        double mass, double radius) :  path(std::move(path)), 
        textureId(std::move(textureId)), name(std::move(name)), 
        r_x(r_x), r_y(r_y), r_z(r_z), v_x(v_x), v_y(v_y), v_z(v_z), 
        mass(mass), radius(radius) {

    f_x = 0.0;
    f_y = 0.0;
    f_z = 0.0;
}

double Body::getRX() { return this->r_x; }
double Body::getRY() { return this->r_y; }
double Body::getRZ() { return this->r_z; }
double Body::getVX() { return this->v_x; }
double Body::getVY() { return this->v_x; }
double Body::getVZ() { return this->v_x; }
double Body::getFX() { return this->f_x; }
double Body::getFY() { return this->f_x; }
double Body::getFZ() { return this->f_x; }
double Body::getMass() { return this->mass; }
double Body::getRadius() { return this->radius; }

void Body::setRX(double r_x) { this->r_x = r_x;}
void Body::setRY(double r_y) { this->r_y = r_y;}
void Body::setRZ(double r_z) { this->r_z = r_z;}
void Body::setVX(double v_x) { this->v_x = v_x;}
void Body::setVY(double v_y) { this->v_y = v_y;}
void Body::setVZ(double v_z) { this->v_z = v_z;}
void Body::setMass(double mass) { this->mass = mass; }

std::wstring Body::toString() {
    return std::wstring(this->textureId.begin(), this->textureId.end())
        + L": r_x = " + std::to_wstring(this->r_x)
        + L", r_y = " + std::to_wstring(this->r_y)
        //+ L", r_z = " + std::to_wstring(this->r_z)
        + L", v_x = " + std::to_wstring(this->v_x)
        + L", v_y = " + std::to_wstring(this->v_y)
        //+ L", v_z = " + std::to_wstring(this->v_z)
        //+ L", mass = " + std::to_wstring(this->mass)
        //+ L", radius = " + std::to_wstring(this->radius)
        ;
}
