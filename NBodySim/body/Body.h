//
// Created by alex on 28.07.2019.
//

#ifndef NBODYSIM_BODY_H
#define NBODYSIM_BODY_H

#include <cmath>
#include <iostream>
#include <memory>
#include <SDL2/SDL.h>
#include "../object/SimpleObject.h"

const double G = 6.67430E-20; // gravitational constant km^3 / kg / sec^2

class Body : public SimpleObject {
public:
    Body(std::string path, std::string textureId, std::wstring name, 
        double r_x, double r_y, double r_z, double v_x, double v_y, double v_z, 
        double mass, double radius = 0.0);

    double distanceTo(const std::shared_ptr<Body> pBody);

    void draw();
    void clean();
    void update(double dt = 0);

    // set the force to 0 for the next iteration
    void resetForce();

    // compute the net force acting between the body a and b, and
    // add to the net force acting on a
    void addForce(const std::shared_ptr<Body> pBody);

    // getters and setters
    double getRX();
    double getRY();
    double getRZ();
    double getVX();
    double getVY();
    double getVZ();
    double getFX();
    double getFY();
    double getFZ();
    double getMass();
    double getRadius();

    void setRX(double r_x);
    void setRY(double r_y);
    void setRZ(double r_z);
    void setVX(double v_x);
    void setVY(double v_y);
    void setVZ(double v_z);
    void setMass(double mass);

    // convert to string representation formatted nicely
    std::wstring toString();

protected:
    std::wstring name; // planet name
    std::string path; // texture path
    std::string textureId; // texture id or Planet name
    double r_x, r_y, r_z; // holds the cartesian positions, km
    double v_x, v_y, v_z; // velocity components, km/s
    double f_x, f_y, f_z; // force components, Newton
    double mass; // mass, kg
    double radius; // radius, km
    int currentRow; // Current row in 
    int currentFrame;

    // For override SimpleObject::update();
    void update();
};

#endif //NBODYSIM_BODY_H
