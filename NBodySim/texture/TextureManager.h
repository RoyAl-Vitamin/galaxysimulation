#ifndef NBODYSIM_TEXTURE_MANAGER_H
#define NBODYSIM_TEXTURE_MANAGER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <map>

// TODO: Переписать на умные указатели
class TextureManager {
public:
    static TextureManager* getInstance();

    bool load(std::string fileName, std::string id, SDL_Renderer* pRenderer);
    // draw static pic
    void draw(std::string id, int x, int y, int width, int height, SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
    // draw fragment pic
    void drawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame, SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);

    std::map<std::string, SDL_Texture*> textureMap;
private:
    inline static TextureManager* pInstance = nullptr;
    TextureManager() {};
};

#endif //NBODYSIM_TEXTURE_MANAGER_H
