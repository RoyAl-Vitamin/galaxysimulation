//
// Created by alex on 04.02.2021.
//

#ifndef NBODYSIM_SIMPLE_OBJECT_H
#define NBODYSIM_SIMPLE_OBJECT_H

#include <string>
#include "../texture/TextureManager.h"

class SimpleObject {
public:
    void draw(SDL_Renderer* pRenderer);
    void update();
    void clean();
    void load(std::string textureId, int x, int y, int width, int height);

protected:
    std::string textureId; // Id for texture map
    std::string path; // Path to texture
    int x, y; // Coordinates on the screen
    int width, height; // width, height for texture
    int currentFrame;
    int currentRow;    
};

#endif //NBODYSIM_SIMPLE_OBJECT_H
