//
// Created by alex on 05.02.2021.
//

#include "SimpleObject.h"

void SimpleObject::draw(SDL_Renderer* pRenderer) {
    TextureManager::getInstance()->drawFrame(textureId, x, y, width, height, currentRow, currentFrame, pRenderer);
}
void SimpleObject::update() {
    
}
void SimpleObject::clean() {

}
void SimpleObject::load(std::string textureId, int x, int y, int width, int height) {

}