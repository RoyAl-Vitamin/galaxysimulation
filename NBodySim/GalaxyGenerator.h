//
// Created by alex on 01.08.2019.
//

#ifndef NBODYSIM_GALAXYGENERATOR_H
#define NBODYSIM_GALAXYGENERATOR_H

#include <vector>
#include "GalaxyType.h"
#include "body/Body.h"

/*
 * Генератор галактики
 * 
 * Mass https://www.fxyz.ru/%D1%81%D0%BF%D1%80%D0%B0%D0%B2%D0%BE%D1%87%D0%BD%D1%8B%D0%B5_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5/%D0%B2%D1%81%D0%B5%D0%BB%D0%B5%D0%BD%D0%BD%D0%B0%D1%8F/%D1%81%D0%BE%D0%BB%D0%BD%D0%B5%D1%87%D0%BD%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0/%D0%BC%D0%B0%D1%81%D1%81%D0%B0_%D0%BF%D0%BB%D0%B0%D0%BD%D0%B5%D1%82_%D1%81%D0%BE%D0%BB%D0%BD%D0%B5%D1%87%D0%BD%D0%BE%D0%B9_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D1%8B/
 * Orbit speed https://studfiles.net/preview/2154642/page:20/
 * Distance https://kvn201.com.ua/distances-of-the-planets-from-the-sun.htm
 */

class GalaxyGenerator {
public:
    // TODO return value may be unique_ptr?
    static std::shared_ptr<std::vector<std::shared_ptr<Body>>> galaxyConstructor(GalaxyType gType_);
};

#endif //NBODYSIM_GALAXYGENERATOR_H
