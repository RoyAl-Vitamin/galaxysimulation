//
// Created by alex on 02.08.2019.
//

#include "Node.h"

void Node::calcInnerNode() {

    if (pBodies->size() <= 1) {
        return;
    }

    auto pBodies000 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies001 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies010 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies011 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies100 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies101 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies110 = std::make_shared<std::vector<std::shared_ptr<Body>>>();
    auto pBodies111 = std::make_shared<std::vector<std::shared_ptr<Body>>>();

    // Распределяем все тела данной ноды по поднодам
    for (auto pLocalBody : *pBodies) {

        if (pLocalBody->getRX() <= x + d / 2 &&
            pLocalBody->getRY() <= y + d / 2 &&
            pLocalBody->getRZ() <= z + d / 2)
        {
            pBodies000->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() > x + d / 2 &&
            pLocalBody->getRY() <= y + d / 2 &&
            pLocalBody->getRZ() <= z + d / 2)
        {
            pBodies100->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() <= x + d / 2 &&
            pLocalBody->getRY() > y + d / 2 &&
            pLocalBody->getRZ() <= z + d / 2)
        {
            pBodies010->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() > x + d / 2 &&
            pLocalBody->getRY() > y + d / 2 &&
            pLocalBody->getRZ() <= z + d / 2)
        {
            pBodies110->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() <= x + d / 2 &&
            pLocalBody->getRY() <= y + d / 2 &&
            pLocalBody->getRZ() > z + d / 2)
        {
            pBodies001->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() > x + d / 2 &&
            pLocalBody->getRY() <= y + d / 2 &&
            pLocalBody->getRZ() > z + d / 2)
        {
            pBodies101->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() <= x + d / 2 &&
            pLocalBody->getRY() > y + d / 2 &&
            pLocalBody->getRZ() > z + d / 2)
        {
            pBodies011->push_back(pLocalBody);
            continue;
        }

        if (pLocalBody->getRX() > x + d / 2 &&
            pLocalBody->getRY() > y + d / 2 &&
            pLocalBody->getRZ() > z + d / 2)
        {
            pBodies111->push_back(pLocalBody);
            continue;
        }
    }

    // Создаём новые ноды с телами
    if (!pBodies000->empty()) {
        pNode000 = std::make_shared<Node>(x, y, z, d / 2, pBodies000);
    } else {
        pBodies000.reset();
    }

    if (!pBodies100->empty()) {
        pNode100 = std::make_shared<Node>(x + d / 2, y, z, d / 2, pBodies100);
    } else {
        pBodies100.reset();
    }

    if (!pBodies010->empty()) {
        pNode010 = std::make_shared<Node>(x, y + d / 2, z, d / 2, pBodies010);
    } else {
        pBodies010.reset();
    }

    if (!pBodies110->empty()) {
        pNode110 = std::make_shared<Node>(x + d / 2, y + d / 2, z, d / 2, pBodies110);
    } else {
        pBodies110.reset();
    }

    if (!pBodies001->empty()) {
        pNode001 = std::make_shared<Node>(x, y, z + d / 2,d / 2, pBodies001);
    } else {
        pBodies001.reset();
    }

    if (!pBodies101->empty()) {
        pNode101 = std::make_shared<Node>(x + d / 2, y, z + d / 2, d / 2, pBodies101);
    } else {
        pBodies101.reset();
    }

    if (!pBodies011->empty()) {
        pNode011 = std::make_shared<Node>(x, y + d / 2, z + d / 2, d / 2, pBodies011);
    } else {
        pBodies011.reset();
    }

    if (!pBodies111->empty()) {
        pNode111 = std::make_shared<Node>(x + d / 2, y + d / 2, z + d / 2, d / 2, pBodies111);
    } else {
        pBodies111.reset();
    }
}

/**
 * Расчитывает центр масс куба текущей ноды
 */
void Node::calcYourself() {
    double r_x = 0, r_y = 0, r_z = 0, mass = 0;
    for (auto pLocalBody : *pBodies) {
        mass += pLocalBody->getMass();
        r_x += pLocalBody->getRX() * pLocalBody->getMass();
        r_y += pLocalBody->getRY() * pLocalBody->getMass();
        r_z += pLocalBody->getRZ() * pLocalBody->getMass();
    }
    r_x /= mass;
    r_y /= mass;
    r_z /= mass;
    if (pBody == nullptr) {
        pBody = std::make_shared<Body>("assets/asteroid.png", "asteroid", L"Астероид", r_x, r_y, r_z, 0, 0, 0, mass);
    } else {
        pBody->setRX(r_x);
        pBody->setRY(r_y);
        pBody->setRZ(r_z);
        pBody->setMass(mass);
    }
}

// TODO Нужен ли копирующий конструктор?
Node::Node(double x, double y, double z, double d, std::shared_ptr<std::vector<std::shared_ptr<Body>>> pBodies) :
        x(x), y(y), z(z), d(d), pBodies(pBodies) {
    pNode000 = nullptr;
    pNode001 = nullptr;
    pNode010 = nullptr;
    pNode011 = nullptr;
    pNode100 = nullptr;
    pNode101 = nullptr;
    pNode110 = nullptr;
    pNode111 = nullptr;
    pBody = nullptr;
}

Node::~Node() {
    this->pNode000.reset();
    this->pNode001.reset();
    this->pNode010.reset();
    this->pNode011.reset();
    this->pNode100.reset();
    this->pNode101.reset();
    this->pNode110.reset();
    this->pNode111.reset();
    this->pBody.reset();
    // TODO возникает утечека памяти, если не удалять вектор, но если удалять, то он удаляет и объекты внутри себя
//    for (auto pLocalBody : *pBodies) {
//        pLocalBody.reset();
//    }
//    pBodies->clear();
    pBodies.reset();
}

std::wstring Node::toString() {
    std::wstring sTemp = L"Node: [" + std::to_wstring(x) + L"][" + std::to_wstring(y) + L"][" + std::to_wstring(z) + L"]";
    if (pBody != nullptr) {
        sTemp += L"\nBody:" + this->pBody->toString();
    }
    if (pNode000 != nullptr) {
        sTemp += L"\nNode[0][0][0]: " + pNode000->toString();
    }
    if (pNode001 != nullptr) {
        sTemp += L"\nNode[0][0][1]: " + pNode001->toString();
    }
    if (pNode010 != nullptr) {
        sTemp += L"\nNode[0][1][0]: " + pNode010->toString();
    }
    if (pNode011 != nullptr) {
        sTemp += L"\nNode[0][1][1]: " + pNode011->toString();
    }
    if (pNode011 != nullptr) {
        sTemp += L"\nNode[0][1][1]: " + pNode011->toString();
    }
    if (pNode100 != nullptr) {
        sTemp += L"\nNode[1][0][0]: " + pNode100->toString();
    }
    if (pNode101 != nullptr) {
        sTemp += L"\nNode[1][0][1]: " + pNode101->toString();
    }
    if (pNode110 != nullptr) {
        sTemp += L"\nNode[1][1][0]: " + pNode110->toString();
    }
    if (pNode111 != nullptr) {
        sTemp += L"\nNode[1][1][1]: " + pNode111->toString();
    }
    return sTemp;
}
