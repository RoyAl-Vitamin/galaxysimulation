//
// Created by alex on 01.08.2019.
//

#ifndef NBODYSIM_GALAXYTYPE_H
#define NBODYSIM_GALAXYTYPE_H


enum GalaxyType {
    // Земля + Луна
    EARTH_AND_MOON,

    // Планеты Солнечной системы
    SOLAR_SYSTEM,

    // Какой-нибудб рандомный тип
    OTHER
};


#endif //NBODYSIM_GALAXYTYPE_H
