#include <vector>
#include <locale>
#include <cstdlib>
#include <SDL2/SDL.h>
#include "body/Body.h"
#include "GalaxyGenerator.h"
#include "GalaxyType.h"
#include "simulator/SecondLawSimulator.h"
#include "simulator/BarnesAndHutSimulator.h"
#include "engine/Engine.h"

const int HEIGHT = 640;
const int WIDTH = 480;

int main(int argc, char* argv[]) {
    auto pEngine = std::make_unique<Engine>();
    //pEngine->init("NBodySim", 100, 100, HEIGHT, WIDTH, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN);
    pEngine->init(L"NBodySim", HEIGHT, WIDTH);
    // SDL_Delay(2000);
    while(pEngine->isRunning()) {
        pEngine->handleEvents();
        pEngine->update();
        pEngine->render();
    }
    pEngine->clean();
    return 0;
}

// TODO Переписать на умные указателя, что бы не было утечки памяти
// Затем можно будте удалять вектор нормально
/*int main() {
    #ifdef linux
        std::locale::global(std::locale("ru_RU.UTF-8"));
    #elif _WIN32
        setlocale(LC_ALL, "russian");
    #else
        #error не удалось установить тип ОС
    #endif
    

    // Кол-во итераций (четверть пути Луны вокруг Земли)
    // 709 часов = 1 оборот Луны вокург Земли
    const int COUNT = 178;
    // Тип моделируемой системы
    const GalaxyType GALAXY_TYPE = EARTH_AND_MOON; // SOLAR_SYSTEM;

    const double hourInSec = 60 * 60;
//    const double dayInSec = hourInSec * 24;
//    const double yearInSec = dayInSec * 365.25;

//    auto *simulator = new SecondLawSimulator(GalaxyGenerator::galaxyConstructor(GALAXY_TYPE));
    auto pSimulator = std::make_unique<BarnesAndHutSimulator>(GalaxyGenerator::galaxyConstructor(GALAXY_TYPE));

    for (auto iter = 0; iter < COUNT; iter++) {
        std::wcout << L"ITERATION  = " << iter << std::endl;
        pSimulator->update(hourInSec);
    }

    // Отчистка памяти
    pSimulator.reset();

//    std::cin.ignore();
//    std::cin.get();
    return 0;
}*/
